import React from "react";

function ProductWidget(props) {
  const { data } = props;
  return (
    <>
      <div class={`m-4 bg-${data.backgroundColor}-200 flex flex-col mr-8`}>
        <div class="h-48 pr-4 pl-10 pt-10 lg:pt-0 flex flex-col lg:flex-row items-center justify-between">
          <div>
            <p class="text-xs font-bold">{data.top.feature}</p>
            <a
              class="text-3xl font-bold no-underline hover:underline"
              href={data.top.title.url}
            >
              {data.top.title.name}
            </a>
            <div>{data.top.description}</div>
          </div>
          <img class="h-12 w-15" src="/img/logo.svg" alt="Some Animation" />
        </div>
        <div class="px-5">
          {data.main.map((section) => (
            <div
              class={`py-6 pl-12 mb-4 bg-${data.backgroundColor}-100 hover:bg-white cursor-pointer rounded-xl flex items-center space-x-4`}
            >
              <div class="flex-shrink-0">
                <img class="h-10 w-12" src="/img/logo.svg" alt="" />
              </div>
              <div>
                <div class="text-2xl font-medium text-black">
                  {section.title}
                </div>
                <p class="text-xs text-gray-500">{section.description}</p>
                <p class="text-gray-500">{section.price}</p>
                <p class="text-xs text-gray-500">{section.priceDescription}</p>
              </div>
              {/* {section.extension && <div>here must be a table</div>} */}
            </div>
          ))}
        </div>
        <div
          class={`p-7 mx-5 mb-6 mt-2 border border-${data.backgroundColor}-400 flex flex-col lg:flex-row items-center justify-between`}
        >
          <div>
            {data.bottom.list.map((item) => (
              <li>{item}</li>
            ))}
          </div>
          <button class="py-3 px-6 bg-yellow-300 hover:bg-yellow-500 cursor-pointer rounded-3xl">
            {data.bottom.buttonText}
          </button>
        </div>
      </div>
    </>
  );
}

export default React.memo(ProductWidget);
