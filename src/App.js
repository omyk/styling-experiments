import React from "react";
import ProductWidget from "./components/ProductWidget";
import Slider from "react-slick";

function App() {
  const AVALIBLE_PRODUCTS = [
    // {
    //   backgroundColor: "yellow",
    //   top: {
    //     feature: "In-Person Access",
    //     title: {
    //       name: "Boxful Self-Storage",
    //       url: "https://www.boxful.com/en/self-storage",
    //     },
    //     description: "Full control of your belongings at Boxful Stores",
    //     image:
    //       "https://www.boxful.com.tw/images/new-home/locker.png.pagespeed.ce.Fn_N6TvTU9.png",
    //   },
    //   main: [
    //     {
    //       title: "Store Locations",
    //       description: "24hr private access to your belongings",
    //       price: "from $494 /month",
    //       priceDescription: "1 free access per month, 1 free pickup trip",
    //       extension: {
    //         type: "table",
    //         data: [],
    //       },
    //     },
    //   ],
    //   bottom: {
    //     list: [
    //       "1-month min storage period",
    //       "No deposit required",
    //       "Best price guarantee",
    //     ],
    //     buttonText: "SEE STORES",
    //   },
    // },
    {
      backgroundColor: "yellow",
      top: {
        feature: "We Come To You",
        title: {
          name: "Boxful Door-to-Door",
          url: "https://www.boxful.com/en/self-storage",
        },
        description: "Effortless storage experience at home",
        image:
          "https://www.boxful.com.tw/images/new-home/locker.png.pagespeed.ce.Fn_N6TvTU9.png",
      },
      main: [
        {
          title: "By-The-Item",
          description: "24hr private access to your belongings",
          price: "from $29 /month",
          priceDescription: "1 free access per month, 1 free pickup trip",
        },
        {
          title: "By-Square-Feet",
          description: "24hr private access to your belongings",
          price: "from $349 /month",
          priceDescription: "1 free access per month, 1 free pickup trip",
        },
      ],
      bottom: {
        list: [
          "We move & deliver for you",
          "Pay monthly for what you use",
          "6Easy online booking & management",
        ],
        buttonText: "SEE PLANS",
      },
    },
    {
      backgroundColor: "green",
      top: {
        feature: "We Come To You",
        title: {
          name: "Boxful Door-to-Door",
          url: "https://www.boxful.com/en/self-storage",
        },
        description: "Effortless storage experience at home",
        image:
          "https://www.boxful.com.tw/images/new-home/locker.png.pagespeed.ce.Fn_N6TvTU9.png",
      },
      main: [
        {
          title: "By-The-Item",
          description: "24hr private access to your belongings",
          price: "from $29 /month",
          priceDescription: "1 free access per month, 1 free pickup trip",
        },
        {
          title: "By-Square-Feet",
          description: "24hr private access to your belongings",
          price: "from $349 /month",
          priceDescription: "1 free access per month, 1 free pickup trip",
        },
      ],
      bottom: {
        list: [
          "We move & deliver for you",
          "Pay monthly for what you use",
          "6Easy online booking & management",
        ],
        buttonText: "SEE PLANS",
      },
    },
    {
      backgroundColor: "red",
      top: {
        feature: "We Come To You",
        title: {
          name: "Boxful Door-to-Door",
          url: "https://www.boxful.com/en/self-storage",
        },
        description: "Effortless storage experience at home",
        image:
          "https://www.boxful.com.tw/images/new-home/locker.png.pagespeed.ce.Fn_N6TvTU9.png",
      },
      main: [
        {
          title: "By-The-Item",
          description: "24hr private access to your belongings",
          price: "from $29 /month",
          priceDescription: "1 free access per month, 1 free pickup trip",
        },
        {
          title: "By-Square-Feet",
          description: "24hr private access to your belongings",
          price: "from $349 /month",
          priceDescription: "1 free access per month, 1 free pickup trip",
        },
      ],
      bottom: {
        list: [
          "We move & deliver for you",
          "Pay monthly for what you use",
          "6Easy online booking & management",
        ],
        buttonText: "SEE PLANS",
      },
    },
    {
      backgroundColor: "pink",
      top: {
        feature: "We Come To You",
        title: {
          name: "Boxful Door-to-Door",
          url: "https://www.boxful.com/en/self-storage",
        },
        description: "Effortless storage experience at home",
        image:
          "https://www.boxful.com.tw/images/new-home/locker.png.pagespeed.ce.Fn_N6TvTU9.png",
      },
      main: [
        {
          title: "By-The-Item",
          description: "24hr private access to your belongings",
          price: "from $29 /month",
          priceDescription: "1 free access per month, 1 free pickup trip",
        },
        {
          title: "By-Square-Feet",
          description: "24hr private access to your belongings",
          price: "from $349 /month",
          priceDescription: "1 free access per month, 1 free pickup trip",
        },
      ],
      bottom: {
        list: [
          "We move & deliver for you",
          "Pay monthly for what you use",
          "6Easy online booking & management",
        ],
        buttonText: "SEE PLANS",
      },
    },
    {
      backgroundColor: "gray",
      top: {
        feature: "We Come To You",
        title: {
          name: "Boxful Door-to-Door",
          url: "https://www.boxful.com/en/self-storage",
        },
        description: "Effortless storage experience at home",
        image:
          "https://www.boxful.com.tw/images/new-home/locker.png.pagespeed.ce.Fn_N6TvTU9.png",
      },
      main: [
        {
          title: "By-The-Item",
          description: "24hr private access to your belongings",
          price: "from $29 /month",
          priceDescription: "1 free access per month, 1 free pickup trip",
        },
        {
          title: "By-Square-Feet",
          description: "24hr private access to your belongings",
          price: "from $349 /month",
          priceDescription: "1 free access per month, 1 free pickup trip",
        },
      ],
      bottom: {
        list: [
          "We move & deliver for you",
          "Pay monthly for what you use",
          "6Easy online booking & management",
        ],
        buttonText: "SEE PLANS",
      },
    },
  ];

  var sliderSettings = {
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    initialSlide: 0,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    responsive: [
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block", background: "red" }}
        onClick={onClick}
      />
    );
  }

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block", background: "green" }}
        onClick={onClick}
      />
    );
  }

  return (
    <>
      {/* <div class="container mx-auto max-w-screen-xl flex flex-col justify-between md:flex-row"> */}
      <div class="container mx-auto max-w-screen-xl">
        <Slider {...sliderSettings}>
          {AVALIBLE_PRODUCTS.map((product) => (
            <ProductWidget data={product} />
          ))}
        </Slider>
      </div>
    </>
  );
}

export default App;
